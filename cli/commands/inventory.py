'''Inventory Command'''
import os
import click
import pylxd
import yaml


@click.command()
@click.pass_context
def inventory(ctx):
    '''Create inventory file'''
    client = pylxd.Client()

    # pylint: disable=no-member
    instances = [client.instances.get(i['name']) for i in
                 ctx.obj['container_configs']]
    ips = [i.state().network['eth0']['addresses'][0]['address'] for i in
           instances]

    target_file = f'{ctx.obj["ansible_dir"]}/inventory.yml'

    try:
        os.remove(target_file)
    except OSError:
        pass

    contents = {'servers': {'hosts': {}}}
    for ip_addr in ips:
        contents['servers']['hosts'][ip_addr] = None

    with open(target_file, 'w') as file:
        yaml.dump(contents, file)
