'''Setup Command'''
import logging
import click
import pylxd


@click.command()
@click.pass_context
def setup(ctx):
    '''Setup Lab'''
    client = pylxd.Client()
    # pylint: disable=no-member
    containers = [c.name for c in client.instances.all()]

    for container in ctx.obj['container_configs']:
        if container_exists(container, containers):
            continue

        try:
            # pylint: disable=no-member
            instance = client.instances.create(container, wait=True)
            instance.start()

        except pylxd.exceptions.LXDAPIException as exception:
            logging.exception(exception)


def container_exists(container, containers):
    '''Check if container exists'''
    return container['name'] in containers
