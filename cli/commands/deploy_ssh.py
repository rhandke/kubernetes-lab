'''Module to deploy SSH files'''
import logging
import pylxd
import click

logger = logging.getLogger(__name__)


@click.command()
@click.pass_context
def deploy_ssh(ctx):
    '''Deploy SSH files to containers'''
    client = pylxd.Client()

    for container in ctx.obj['container_configs']:
        try:
            # pylint: disable=no-member
            instance = client.instances.get(container['name'])

            with open(f'{ctx.obj["data_dir"]}/authorized_keys', 'r') as file:
                data = file.read()
                target = '/home/ubuntu/.ssh/authorized_keys'
                instance.files.put(filepath=target, data=data)

        except pylxd.exceptions.LXDAPIException as exception:
            logger.exception(exception)
