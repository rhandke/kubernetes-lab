'''Module to check the lab status'''
import logging
import pylxd
import click
from rich.table import Table
from rich.console import Console

logger = logging.getLogger(__name__)


@click.command()
@click.pass_context
def status(ctx):
    '''Check lab status'''
    client = pylxd.Client()

    try:
        # pylint: disable=no-member
        instances = [client.instances.get(i['name']) for i in
                     ctx.obj['container_configs']]

        table = Table()
        table.add_column('Name', style='blue', no_wrap=True)
        table.add_column('Status', no_wrap=True)

        for instance in instances:
            table.add_row(instance.name, instance.status)

        console = Console()
        console.print(table)

    except pylxd.exceptions.LXDAPIException:
        click.echo("Lab down: One or more container not running")
