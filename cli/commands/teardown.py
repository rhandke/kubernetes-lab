'''Teardown Command'''
import logging
import pylxd
import click


@click.command()
@click.pass_context
def teardown(ctx):
    '''Teardown lab'''
    client = pylxd.Client()
    # pylint: disable=no-member
    containers = client.instances.all()

    container_names = [c['name'] for c in ctx.obj['container_configs']]

    for container in containers:
        if container.name in container_names:
            try:
                # pylint: disable=no-member
                instance = client.instances.get(container.name)
                instance.stop()
            except pylxd.exceptions.LXDAPIException as exception:
                logging.exception(exception)
