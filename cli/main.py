'''Command Group'''
import os
import json
from pathlib import Path
import logging
from rich.logging import RichHandler
import click

FORMAT = "%(message)s"
logging.basicConfig(
    level=logging.ERROR, format=FORMAT, datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True)])

COMMAND_DIR = os.path.join(os.path.dirname(__file__), 'commands')


class KubernetesLab(click.MultiCommand):
    '''Provisioning lab containers'''
    def list_commands(self, ctx):
        commands = []

        for filename in os.listdir(COMMAND_DIR):
            file_name, file_extension = os.path.splitext(filename)

            if file_name == '__init__' or file_extension != '.py':
                continue

            commands.append(file_name)

        commands.sort()
        return commands

    def get_command(self, ctx, cmd_name):
        command = {}
        filename = os.path.join(COMMAND_DIR, cmd_name + '.py')

        with open(filename, 'r') as file:
            code = compile(file.read(), filename, 'exec')
            eval(code, command, command)  # pylint: disable=eval-used
        return command[cmd_name]


@click.command(cls=KubernetesLab)
@click.pass_context
def cli(ctx):
    '''
    Kubernetes Lab - Create a lab to learn kubernetes
    Copyright (C) 2021  Roman Handke

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    '''
    ctx.ensure_object(dict)
    ctx.obj['command_dir'] = COMMAND_DIR

    path = Path(__file__)
    ctx.obj['cli_dir'] = path.parent.absolute()
    ctx.obj['root_dir'] = path.parent.parent.absolute()
    ctx.obj['data_dir'] = f'{ctx.obj["root_dir"]}/data'
    ctx.obj['ansible_dir'] = f'{ctx.obj["root_dir"]}/ansible'

    with open(f'{ctx.obj["root_dir"]}/containers.json', 'r') as container_file:
        containers = json.load(container_file)
    ctx.obj['container_configs'] = containers
