#!/usr/bin/python3
'''Provisioning LXCs'''

import os
import logging
import subprocess

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

CWD = os.getcwd()
LXC_TEMPLATE = 'ubuntu:20.04'
CONTAINERS = [
    'ubuntu01',
    'ubuntu02',
    'ubuntu03',
    'ubuntu04',
]

def deploy():
    '''Provisioning LXCs'''
    create_containers()


def create_containers():
    '''Create LXCs'''
    for container in CONTAINERS:
        if container_exists(container):
            continue

        try:
            cmd = f"lxc launch {LXC_TEMPLATE} {container}"
            subprocess.run(cmd, shell=True, check=True, capture_output=True)

            cmd = f"lxc file push {CWD}/authorized_keys {container}/home/ubuntu/.ssh/"
            result = subprocess.run(cmd, shell=True, check=True, capture_output=True)
            logger.debug(result.stdout)
        except subprocess.CalledProcessError as error:
            logger.error(error.stderr)


def container_exists(container):
    '''Check if container exists'''
    result = subprocess.run(f"lxc list | grep {container}",
            shell=True, check=False, capture_output=True)
    return not result.returncode


if __name__ == '__main__':
    deploy()
