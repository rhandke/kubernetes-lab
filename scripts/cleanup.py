#!/usr/bin/python3
'''Remove LXCs'''

import subprocess

CONTAINERS = [
    'ubuntu01',
    'ubuntu02',
    'ubuntu03',
    'ubuntu04',
]


def cleanup():
    '''Cleanup'''
    for container in CONTAINERS:
        subprocess.run(f"lxc delete -f {container}", shell=True, check=True)


if __name__ == '__main__':
    cleanup()
