# Kubernetes Lab

Create a lab using LXC and Ansible which can be used to learn Kubernetes.

# Requirements
* [Python3](https://www.python.org/download/releases/3.0/)
* [LXC - Linux Containers](https://linuxcontainers.org/)
* [Ansible](https://www.ansible.com/)

# Installation
Clone this repository

```console
git clone git@gitlab.com:rhandke/kubernetes-lab.git
```

Provisioning and controlling the LXCs is handled via a Python3 CLI. To get it up and running use

```console
virtualenv venv \
pip3 install .
source venv/bin/activate
```

# Configuration
The containers.json file can be modified to control the containers the lab will contain

# Usage
Get a list of available commands
```
lab
```

Firstly create a lab consisting of ephemeral LXCs
```
lab setup
```

Deploy the authorized_keys file to the new containers
```
lab deploy_ssh
```
The default data/authorized_keys can be customized

Create an inventory file contaning the ips of the lab's containers
```
lab inventory
```
Will create/overwrite the inventory.yml in the ansible/ directory

Check if Ansible can execture commands on the containers by using the ping module
```
cd ansible
ansible servers -m ping
```

Check the status of the lab
```
lab status
```
At the moment the lab will be considered down if one or more containers are not running.
